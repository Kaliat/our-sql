exec: main.o fonction.o KyllianToolsLib.o ESLib.o ErreurLib.o
	gcc -Wall main.o fonction.o KyllianToolsLib.o ESLib.o ErreurLib.o -o exec -lm

main.o: main.c 
	gcc -c -Wall main.c

KyllianToolsLib.o: MyLib/KyllianToolsLib.c MyLib/KyllianToolsLib.h
	gcc -c -Wall MyLib/KyllianToolsLib.c

fonction.o: fonction.c fonction.h MyLib/KyllianToolsLib.h ESLib.h
	gcc -Wall -c fonction.c

ESLib.o: ESLib.c ESLib.h ErreurLib.h
	gcc -Wall -c ESLib.c

ErreurLib.o: ErreurLib.c ErreurLib.h
	gcc -Wall -c ErreurLib.c
