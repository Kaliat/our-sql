#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "fonction.h"
#include <time.h>
#include "math.h"
#include <assert.h>
#include "MyLib/KyllianToolsLib.h"



int main(int argc,char* argv[]){
	// ------------------------------------------------------- //
	Table T;
	int size = strlen(argv[0])+1;
	char* command = malloc(sizeof(char)*size);
	strcpy(command,argv[0]);
	for(int i=1;i<argc;i++){
		size += 1 + strlen(argv[i]);
		command = realloc(command,sizeof(char)*size);
		strcat(command," ");
		strcat(command,argv[i]);
	}
	printf(TXT_MAGENTA);
	printf("command executed : %s\n",command);
	printf(TXT_END_COLOR);
	if(str_same(argv[1],"SELECT")){
		T = databaseQuery(command);
		afficheBDD(T);
		libereTable(T);
	}
	else{
		databaseQuery(command);
	}
	free(command);
}

// ANCIENS MAINS
/*

// ------------------------------------------------------- //
	bool invalidRequest = true;
	getArgsValue(argc,argv,&args);
	char *fcontents;
	char *dbname;
	char **selected;
	//SWITCH SUR TYPES DE COMMANDES PRINCIPALES UPDATE SELECT INSERT DELETE
	switch(args[1]){
		case SELECT:	
		dbname = argv[4];
		selected = getSelectedColumnsName(argv[2]);
		fcontents = malloc(sizeof(char)*DBSIZE);
		if(getDbText(fcontents,dbname) == 0){ 	//SI LE FICHIER SPECIFIE EXISTE
			char* backupFC = malloc(sizeof(char)*strlen(fcontents));
			strcpy(backupFC,fcontents);
			char** contentbyline;
			contentbyline = str_split(fcontents,'\n');
			if(!strcmp(contentbyline[0],"<BDD-START>")){
				char **nomCol = str_split(contentbyline[1],',');
				printf(TXT_BLUE);
				bool showCol[30]; //TODO ALLOC DYNAMIQUE
				int nbToCheck = 0;
				//
				if(selected == ALL_SELECTED){
					for(int i2=0;i2<30;i2++){
						showCol[i2] = true;
					}
				}
				else{
					for(int i2=0;i2<30;i2++){
						showCol[i2] = false;
					}
					for(int i=0;selected[i];i++){
						nbToCheck++;
						for(int i2=1;nomCol[i2];i2++){
							//printf("%s =? %s\n",selected[i],nomCol[i2]);
							if(!strcmp(selected[i],nomCol[i2])){
								//printf("true\n");
								showCol[i2-1] = true;
							}
						}
					}
				}
				for(int i=0;nomCol[i+2];i++){
					if(showCol[i]){
						printf("%s\t\t",nomCol[i+1]);
					}
				}
				printf("\n");
				//printf("%s\t\t|%s\t\t|%s\t|%s\n",nomCol[1],nomCol[2],nomCol[3],nomCol[4]);
				printf(TXT_CYAN);
				for (int i = 2; contentbyline[i]; i++){
					removeChar(contentbyline[i],'{');
					removeChar(contentbyline[i],'}');
					char** line = str_split(contentbyline[i],',');
					if(strcmp(contentbyline[i],"<BDD-END>")){
						for(int i2 = 0;line[i2];i2++){
							//printf("showCol[%d] = %d\n",i2,showCol[i2]);
							if(showCol[i2]){
								removeChar(line[i2],'"');
								printf("%s\t\t",line[i2]);
							}
						}
						printf("\n");
					}
				}
				printf(TXT_END_COLOR);
			}
		}
		else{
			printf(TXT_RED);
			printf("t'es un pd\n");
			printf(TXT_END_COLOR);
		}
		invalidRequest = false;
		
		break;
		
		case INSERT:
		dbname = argv[3];
		char *values = argv[5];
		fcontents = malloc(sizeof(char)*DBSIZE);
		if(getDbText(fcontents,dbname) == 0){
			char* backupFC = malloc(sizeof(char)*strlen(fcontents));
			strcpy(backupFC,fcontents);
			char** contentbyline;
			contentbyline = str_split(fcontents,'\n');
			int nbLine = 0;
			if(!strcmp(contentbyline[0],"<BDD-START>")){
				for (int i = 2; contentbyline[i]; i++){
					if(!strcmp(contentbyline[i],"<BDD-END>")){
						nbLine = i + 1;
					}
				}
			}
			char* lineInsert = malloc(sizeof(char)*strlen(values)+2);
			sprintf(lineInsert,"{%s}",values);
			contentbyline = putLineAt(str_split(backupFC,'\n'),lineInsert,nbLine - 1,nbLine);
			writeBdd(dbname,contentbyline,nbLine+1);
		}
		else{
			printf(TXT_RED);
			printf("t'es un pd\n");
			printf(TXT_END_COLOR);
		}
		invalidRequest = false;
		
		break;
		//TEST
		case ALL:
			dbname = argv[4];
			selected = getSelectedColumnsName(argv[2]);
			Table T = getTableFromFile(dbname);
			Table T2 = shrinkTable(T,selected);
			afficheTable(T2);
			invalidRequest = false;
		break;
	}
	if(invalidRequest){
		printf(TXT_RED);
		printf("ERROR : INVALID ARGUMENTS : ");
		printf(TXT_YELLOW);
		printf("\"\%s\" ",argv[1]);
		for(int i=2;i<argc;i++){
			printf(TXT_RED);
			printf("AND ");
			printf(TXT_YELLOW);
			printf("\"%s\" ",argv[i]);
		}
		printf("\n");
		printf(TXT_END_COLOR);
	}
	// ------------------------------------------------------- //










*/














