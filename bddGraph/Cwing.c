#ifdef _WIN32
	#include <windows.h>
#endif

#ifdef __APPLE__
//	#include <OpenGL/gl.h>
//	#include <OpenGL/CGLMacro.h>
	#include <GLUT/glut.h>		// Header File For The GLut Library
#else
	#include <GL/glut.h>			// Header File For The GLut Library
	#ifdef _WIN32
		#include <GL/glext.h>
	#else
		#include <X11/Xlib.h>
		#include <GL/glx.h>
	#endif
#endif

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "GfxLib/GfxLib.h"
#include "GfxLib/BmpLib.h"
#include "GfxLib/ESLib.h"
#include "../fonction.h"
#include "../MyLib/KyllianToolsLib.h"
#include "../MyLib/KToolsLibForGfxLib.h"
#include "Cwing.h"

// FONCTIONS

int x1cursor,y1cursor;
int x2cursor,y2cursor;
int x = 0,y = 0;
float xslide,yslide;
bool xdraggable = false,ydraggable = false;
Table T;
int tableToLoad = 1;
bool keyboardState[255];
bool oneSelected = false;
int mouseState = 0;
int AS = 0;
int OS = 0;
bool handleSouris = false;
textField** TXTF = NULL;
int nblignetextfield = 0;
int nbcolonnetextfield = 0;

textField initTextField(int xx,int yy,int width,int height,RGB couleur,char* chaine){
	textField B;
	B.width = width;
	B.height = height;
	B.couleur = couleur;
	B.skin = NULL;
	B.texte = malloc(sizeof(char)*100);
	B.tailleTexte = height*0.7;
	B.editing = false;
	strcpy(B.texte,chaine);
	B.x = xx;
	B.y = yy;
	
	return B;
}

void afficheTextField(textField *B){
	if(B->skin == NULL){
		couleurCourante(0,0,0);
		rectangle(B->x - x,B->y - y,B->x+B->width - x,B->y+B->height - y);
		if(B->editing){
			couleurCourante(B->couleur.r + 127,B->couleur.g+127,B->couleur.b+127);
		}
		else{
			couleurCourante(B->couleur.r,B->couleur.g,B->couleur.b);
		}
		rectangle(B->x+2 - x,B->y +2 - y,B->x+B->width - x-2,B->y+B->height - y-2);
		couleurCourante(0,0,0);
		afficheChaine(B->texte,B->tailleTexte,B->x+B->width/2-tailleChaine(B->texte,B->tailleTexte)/2-x,B->y+B->height/2-B->tailleTexte/2-y);
	}
	if(B->skin != NULL){
		
	}
	if(tailleChaine(B->texte,B->tailleTexte) > B->width*0.9){
		int i;
		for(i= B->height*0.7;tailleChaine(B->texte,i) > B->width*0.9;i--){}
		B->tailleTexte = i;
	}
	if(tailleChaine(B->texte,B->tailleTexte) < B->width*0.9 && B->tailleTexte < B->height*0.7){
		int i;
		for(i= B->height*0.7;tailleChaine(B->texte,i) > B->width*0.9;i--){}
		B->tailleTexte = i;
	}
	if(B->showCursor && B->editing){
		x1cursor = B->x + B->width/2 + tailleChaine(B->texte,B->tailleTexte)/2 -x;
		y1cursor = B->y + B->height/2 - B->tailleTexte/2 -y - 5;
		x2cursor = B->x + B->width/2 + tailleChaine(B->texte,B->tailleTexte)/2 + 10-x;
		y2cursor = B->y + B->height/2 - B->tailleTexte/2 + B->tailleTexte-y + 5;
	}
}

void afficheAllTxtFields(){
	for(int i=0;i<nblignetextfield;i++){
		for(int i2=0;i2<nbcolonnetextfield;i2++){
			afficheTextField(&(TXTF[i][i2]));
		}
	}
}

void initTabTextField (Table T){
	nblignetextfield = T.nbligne+1;
	nbcolonnetextfield = T.nbcolonne;
	TXTF = malloc(sizeof(textField*)*(nblignetextfield));
	for(int i=0;i<nblignetextfield;i++){
		TXTF[i] = malloc(sizeof(textField)*nbcolonnetextfield);
	}
	int spacex = 200,spacey = 40;	// Elon et Kevin seraient fiers
	int posx = 10;
	int posy = HF-40;
	for(int i=0;i<T.nbcolonne;i++){
		TXTF[0][i] = initTextField(posx,posy,spacex,spacey,(RGB){255,0,0},T.NomColonnes[i]);
		posx+=spacex;
	}
	posx = 10;
	posy -= spacey;
	for(int i=0;i<T.nbligne;i++){
		for(int i2=0;i2< T.nbcolonne;i2++){
			TXTF[i+1][i2] = initTextField(posx,posy,spacex,spacey,(RGB){227,227,227},(T.T)[i][i2]);
			posx+= spacex;
		}
		posx = 10;
		posy-= spacey;
	}
}



void textFieldClickListener (textField *B){
	if(handleSouris){
		if (mouseState == MOUSE_LEFT_UP){
			if(AS > B->x-x && AS < B->x+B->width-x && OS> B->y-y && OS < B->y+B->height-y){
				B->editing = true;
				oneSelected = true;
			}
			else{
				if(B->editing){
					oneSelected = false;
				}
				B->editing = false;
			}
		}
	}
}

//BOUTONS


void addEventListener (bouton *B,char* listener,void (*event) (void)){
	B->event = event;
	strcpy(B->listener,listener);
}

bouton initBouton(int width,int height,char* texte,RGB couleur){
	bouton B;
	B.width = width;
	B.height = height;
	B.couleur = couleur;
	B.skin = NULL;
	B.texte = texte;
	int i;
	for(i= height*0.7;tailleChaine(B.texte,i) > width*0.9;i--){}
	B.tailleTexte = i;
	
	B.addEventListener = addEventListener;
	return B;
}

bouton initBoutonFromImg(char* chemin,RGB fondVert){
	bouton B;
	B.skin = lisBMPRGB(chemin);
	B.couleur = fondVert;
	B.width = B.skin->largeurImage;
	B.height = B.skin->hauteurImage;
	B.addEventListener = addEventListener;
	return B;
}

void afficheBouton(int x,int y,bouton *B){
	if(B->skin == NULL){
		couleurCourante(B->couleur.r,B->couleur.g,B->couleur.b);
		rectangle(x,y,x+B->width,y+B->height);
		couleurCourante(0,0,0);
		afficheChaine(B->texte,B->tailleTexte,x+B->width/2-tailleChaine(B->texte,B->tailleTexte)/2,y+B->height/2-B->tailleTexte/2);
	}
	if(B->skin != NULL){
		
	}
	B->x = x;
	B->y = y;
}

void executeActionBouton (bouton B){
	if(B.listener[0] == 'c' && B.listener[1] == 'l' && B.listener[2] == 'i'){
		if(handleSouris){
			if (mouseState == MOUSE_LEFT_UP){
				if(AS > B.x && AS < B.x+B.width && OS> B.y && OS < B.y+B.height){
					B.event();
				}
			}
		}
	}
	
	// PAS ADAPTE AUX NOUVEAUX EVENTS I/O SUR GFXLIB
	/*if(B.listener[0] == 'k' && B.listener[1] == 'e' && B.listener[2] == 'y' && B.listener[3] == '_'){
		if(evenement == Clavier){
			if(caractereClavier() == B.listener[4]){
				B.event();
			}
		}
	}*/
}
