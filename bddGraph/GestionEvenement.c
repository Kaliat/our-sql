#ifdef _WIN32
	#include <windows.h>
#endif

#ifdef __APPLE__
//	#include <OpenGL/gl.h>
//	#include <OpenGL/CGLMacro.h>
	#include <GLUT/glut.h>		// Header File For The GLut Library
#else
	#include <GL/glut.h>			// Header File For The GLut Library
	#ifdef _WIN32
		#include <GL/glext.h>
	#else
		#include <X11/Xlib.h>
		#include <GL/glx.h>
	#endif
#endif

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "GfxLib/GfxLib.h"
#include "GfxLib/BmpLib.h"
#include "GfxLib/ESLib.h"
#include "../fonction.h"
#include "../MyLib/KyllianToolsLib.h"
#include "../MyLib/KToolsLibForGfxLib.h"
#include "../MyLib/exploFichier.h"
#include "Cwing.h"

#define SELECTED_FILE bdd2.txt
#define NOM_PROGRAMME "Our-Sql"

void gestionEvenement(EvenementGfx evenement);
int main(int argc, char **argv){
	initialiseGfx(argc, argv);
	prepareFenetreGraphique(NOM_PROGRAMME, 1600, 900);
	lanceBoucleEvenements();
	return 0;
}

//FONCTION DE GESTION DES CLICKS ET DEPLACEMENTS DE LA SOURIS
void gestionSouris (int button,int state,int xa,int ya){
	switch (button){
		case GLUT_LEFT_BUTTON :
			//LEFT CLICK
			switch(state){
				case GLUT_UP:
					//PRESSED
					mouseState = MOUSE_LEFT_UP;
				break;
				case GLUT_DOWN:
					//UNPRESSED
					mouseState = MOUSE_LEFT_DOWN;
				break;
			}
		break;
		case 3 :
			//SCROLLING UP
			mouseState = MOUSE_SCROLL_UP;
		break;
		case 4 :
			//SCROLLING DOWN
			mouseState = MOUSE_SCROLL_DOWN;
		break;
		case GLUT_RIGHT_BUTTON :
			//RIGHT CLICK
			switch(state){
				case GLUT_UP:
					//PRESSED
					mouseState = MOUSE_RIGHT_UP;
				break;
				case GLUT_DOWN:
					//UNPRESSED
					mouseState = MOUSE_RIGHT_DOWN;
				break;
			}
		break;
	}
	AS = xa;
	OS = HF - ya;
	handleSouris = true;
}
//CREE PAR KYLLIAN MARIE EN CIR2 A L'ISEN TOULON

//FONCTION CALLBACK DE MOTION DE LA SOURIS 
void motion (int xa,int ya){
	AS = xa;
	OS = LF - ya - 100;
}
///CREE PAR KYLLIAN MARIE EN CIR2 A L'ISEN TOULON

//FONCTION CALLBACK DE PRESSION CLAVIER
void keyDown (unsigned char a,int x,int y){
	bool selected = false;
	for(int i=0;i<nblignetextfield;i++){
		for(int i2=0;i2<nbcolonnetextfield;i2++){
			if(TXTF[i][i2].editing){
				selected = true;
				switch(a){
					case 13:
						TXTF[i][i2].editing = false;
						oneSelected = false;
					break;
					case 8:
						TXTF[i][i2].texte[strlen(TXTF[i][i2].texte)-1] = '\0';
						TXTF[i][i2].texte[strlen(TXTF[i][i2].texte)] = '\0';
					break;
					default:
						if(true){
							char stra[2];
							stra[0] = a;
							stra[1] = '\0';
							strcat(TXTF[i][i2].texte,stra);
						}
					break;
				}
			}
		}
	}
	if(!selected){
		keyboardState[a] = true;
	}
}
//CREE PAR KYLLIAN MARIE EN CIR2 A L'ISEN TOULON

//FONCTION CALLBACK DE RELACHEMENT CLAVIER
void keyUp (unsigned char a,int x,int y){
	keyboardState[a] = false;
}
//CREE PAR KYLLIAN MARIE EN CIR2 A L'ISEN TOULON

//UNUSED REMPLACEE PAR afficheAllTxtFields() // FONCTION D AFFICHAGE D UNE TABLE OURSQL EN GRAPHIQUE
void afficheTableGraph (Table T){
	int spacex = 200,spacey = 40;	// Elon et Kevin seraient fiers
	int size = 16;
	int posx = 10;
	int posy = HF-40;
	for(int i=0;i<T.nbcolonne;i++){
		couleurCourante(0,0,0);
		ligne(posx-x,-9999-y,posx-x,9999-y);
		couleurCourante(255,0,0);
		afficheChaine(T.NomColonnes[i],size,posx-x,posy-y);
		posx+=spacex;
	}
	couleurCourante(0,0,0);
	ligne(0-x,posy-y,99999-x,posy-y);
	couleurCourante(0,0,0);
	posx = 10;
	posy -= spacey;
	for(int i=0;i<T.nbligne;i++){
		for(int i2=0;i2< T.nbcolonne;i2++){
			if(posx-x > 0 && posx-x < LF){
				afficheChaine((T.T)[i][i2],size,posx-x,posy-y);
			}
			posx+= spacex;
		}
		couleurCourante(0,0,0);
		if(posy-y > 0 && posy-y < LF){
			ligne(0-x,posy-y,99999-x,posy-y);
		}
		posx = 10;
		posy-= spacey;
	}
}
//CREE PAR KYLLIAN MARIE EN CIR2 A L'ISEN TOULON

//FONCTION DE REMPLISSAGE DES TEXTFIELDS AVEC LE CONTENU D UNE TABLE OURSQL
Table createTableFromGFX (){
	Table Tn;
	Tn.nbcolonne = nbcolonnetextfield;
	Tn.nbligne = nblignetextfield - 1;
	Tn.NomColonnes = malloc(sizeof(char*)*nbcolonnetextfield);
	Tn.typelist = malloc(sizeof(int)*Tn.nbcolonne);
	Tn.T = malloc(sizeof(char**)*(Tn.nbligne));
	for(int i=0;i<Tn.nbligne;i++){
		Tn.T[i] = malloc(sizeof(char*)*Tn.nbcolonne);
	}
	for(int i =0;i<Tn.nbcolonne;i++){
		Tn.NomColonnes[i] = malloc(sizeof(char)*strlen(TXTF[0][i].texte));
		strcpy(Tn.NomColonnes[i],TXTF[0][i].texte);
		Tn.typelist[i] = typeSTR;
	}
	for(int i =0;i<Tn.nbligne;i++){
		for(int i2 = 0;i2<Tn.nbcolonne;i2++){
			(Tn.T)[i][i2] = malloc(sizeof(char)*strlen(TXTF[i+1][i2].texte));
			strcpy(Tn.T[i][i2],TXTF[i+1][i2].texte);
		}
	}
	return Tn;
}
//CREE PAR KYLLIAN MARIE EN CIR2 A L'ISEN TOULON

//FONCTION CALLBACK DU BOUTON ENREGISTRER
void actionOfBtEnregistrer (){
	Table ret = createTableFromGFX();
	writeBdd("bddgraph.txt",str_split(writeInnerTextFromTable(ret),'\n'),ret.nbligne+3);
}
//CREE PAR KYLLIAN MARIE EN CIR2 A L'ISEN TOULON

//FONCTION CALLBACK DU BOUTON AJOUT DE COLONNE
void actionOfBtAddColumnt (){
	Table ret = createTableFromGFX();
	char** strtab = malloc(sizeof(char*)*2);
	strtab[0] = malloc(sizeof(char)*5);
	strtab[1] = false;
	strcpy(strtab[0],"null");
	int Tab[5] = {typeSTR};
	ret = enlargeTable(ret,strtab,Tab);
	betterFree(&strtab);
	initTabTextField(ret);
}
//CREE PAR KYLLIAN MARIE EN CIR2 A L'ISEN TOULON

//FONCTION CALLBACK DU BOUTON AJOUT DE LIGNE
void actionOfBtAddLine (){
	Table ret = createTableFromGFX();
	ret.nbligne++;
	ret.T = realloc(ret.T,sizeof(char**)*ret.nbligne);
	ret.T[ret.nbligne-1] = malloc(sizeof(char*)*ret.nbcolonne);
	for(int i=0;i<ret.nbcolonne;i++){
		ret.T[ret.nbligne-1][i] = malloc(sizeof(char)*strlen("null"));
		strcpy(ret.T[ret.nbligne-1][i],"null");
	}
	initTabTextField(ret);
}
//CREE PAR KYLLIAN MARIE EN CIR2 A L'ISEN TOULON

void gestionEvenement(EvenementGfx evenement)
{
	// DECLARATION DES VARIABLES
	static int coef = 2; 					//combien de fois par seconde le curseur clignote
	static bool first = true;				//premiere boucle ?
	static double t0 ;						//temps reel au demarage du programme
	static double cpt;						//variable pour le clignottement du curseur		
	static int intcpt = 0;					//variable pour le clignottement du curseur
	static int oldcpt;						//variable pour le clignottement du curseur
	static bool afficheCurseur = false;		//affichage du curseur ?
	static DonneesImageRGB* fond;			//fond oursql a l entree du programme
	static bool mode = 0;					//mode d'affichage, menu principal ou programme
	static bouton bt_enregistrer;			//bouton enregistrer
	static bouton bt_AddColumn;				//bouton ajout de colonne
	static bouton bt_AddLine;				//bouton ajout de ligne
	static exploFichier EX;					//explorateur de fichiers
	static char* filePath = false;			//chemin initial de l explorateur fichier

	//EXECUTES A CHAQUE BOUCLE 
	if(first){
		t0 = tempsReel();
		first = false;
	}
	cpt = (tempsReel() - t0) * coef;
	oldcpt = intcpt;
	intcpt = (int) cpt;
	if(intcpt != oldcpt){
		afficheCurseur = !afficheCurseur;
	}
	for(int i=0;i<nblignetextfield;i++){
		for(int i2=0;i2<nbcolonnetextfield;i2++){
			textFieldClickListener(&(TXTF[i][i2]));
		}
	}
	executeActionBouton(bt_enregistrer);
	executeActionBouton(bt_AddColumn);
	executeActionBouton(bt_AddLine);
	
	// SWITCH EVENEMENT
	switch (evenement)
	{
		case Initialisation:
			//FONCTIONS CALLBACK GLUT
			glutMouseFunc(gestionSouris);
			glutMotionFunc(motion);
			glutPassiveMotionFunc(motion);
			glutKeyboardFunc(keyDown);
			glutKeyboardUpFunc(keyUp);
			glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);
			for(int i=0;i<255;i++){
				keyboardState[i] = false;
			}

			demandeTemporisation(0);
			xslide = 30;
			yslide = HF-30;
			T = databaseQuery("./exec SELECT ALL FROM ../modulePhys2CIR2G1.osql INNER_JOIN ../modulePhys2CIR2G2.osql ON id"); //TABLE INITIALE
			initTabTextField(T);	//copie de la table dans les textfields
			//INITIALISATION DES BOUTONS 
			bt_enregistrer = initBouton(90,30,"Save",(RGB){255,0,0});	
			bt_enregistrer.addEventListener(&bt_enregistrer,"click",actionOfBtEnregistrer);
			bt_AddColumn = initBouton(30,30,"+",(RGB){0,255,255});
			bt_AddColumn.addEventListener(&bt_AddColumn,"click",actionOfBtAddColumnt);
			bt_AddLine = initBouton(30,300,"+",(RGB){0,255,255});
			bt_AddLine.addEventListener(&bt_AddLine,"click",actionOfBtAddLine);
			//INITIALISATION DU FOND
			fond = lisBMPRGB("Important/happiness.bmp");
			fond = resizeDonneesImageRGB(&fond,LF);
			EX = initExploFichier("/home");
		break;
		
		case Temporisation:
			rafraichisFenetre();
			gereExploFichier(&EX);
			gereClicExploFichier(&EX,&filePath,AS,OS,handleSouris && mouseState==MOUSE_LEFT_UP);
			if(filePath){	//si le filepath existe
				char command[200];
				sprintf(command,"./exec SELECT ALL FROM %s",filePath);
				T = databaseQuery(command);
				initTabTextField(T);
				filePath = false;
				EX.isShown = false;		//disparition de explofichier
			}
			//GESTION DES SCROLLBARS
			if(xdraggable){
				if(AS > 30 && AS < LF-30){
					xslide = AS;
				}
				if(AS <30){
					xslide = 30;
				}
				if(AS > LF-30){
					xslide = LF-30;
				}
			}
			if(ydraggable){
				if(OS > 30 && OS < HF-30){
					yslide = OS;
				}
				if(OS <30){
					yslide = 30;
				}
				if(OS > HF-30){
					yslide = HF-30;
				}
			}
			x = (xslide -30) * 3;
			y = (yslide *1./ HF) * (nblignetextfield*30);
		break;
			
		case Affichage:
			// RENDERING FRAME
			afficheExploFichier(EX);
			if(mode == 0){
				ecrisImageShort(fond,0,0);
			}
			else{
				effaceFenetre (255,255,255);
				afficheAllTxtFields();

				if(afficheCurseur && oneSelected){	//SURBRILLANCE DE LA CASE SELECTIONNEE
					couleurCourante(0,0,0);
					rectangle(x1cursor,y1cursor,x2cursor,y2cursor);
				}

				//AFFICHAGE DES BOUTONS
				afficheBouton(0,30,&bt_enregistrer);
				afficheBouton(LF-60,HF-30,&bt_AddColumn);
				afficheBouton(0,100,&bt_AddLine);

				//SCROLLBAR HORIZONTALE
				couleurCourante(127,127,127);
				rectangle(0,0,LF,30);
				couleurCourante(0,0,0);
				rectangle(xslide-30,0,xslide+30,30);
				
				//SCROLLBAR VERTICALE
				couleurCourante(127,127,127);
				rectangle(LF-30,0,LF,HF);
				couleurCourante(0,0,0);
				rectangle(LF-30,yslide,LF,yslide + (HF)/(nblignetextfield*30)*HF);
			}
		break;
			
		case Clavier:
			//UNUSED UTILISATION DE KeyboardState
		break;

		case BoutonSouris:
			//UNUSED UTILISATION DE handleSouris
		break;
		
		case ClavierSpecial:
			//UNUSED UTILISATION DE KeyboardState
		break;
			
		case Souris:
			//UNUSED UTILISATION DE handleSouris
		break;
		
		case Inactivite:
			// PAS D ENTREE
		break;
		
		case Redimensionnement:
			// REDIMENSIONNEMENT DE LA FENETRE
		break;
	}
	if(handleSouris){
			if (mouseState == MOUSE_LEFT_DOWN)
			{
				if(AS> xslide-30 && AS< xslide+30 && OS>0 && OS<30){
					xdraggable = true;
				}
				if(AS> LF-30 && AS< LF && OS>yslide-30 && OS<yslide+30){
					ydraggable = true;
				}
			}
			if (mouseState == MOUSE_LEFT_UP){
				xdraggable = false;
				ydraggable = false;
				mode++;
			}
			if(mouseState == MOUSE_SCROLL_DOWN){
					yslide -= 30;
			}
			if(mouseState == MOUSE_SCROLL_UP){
				if(yslide < HF-30){
					yslide += 30;
				}
			}
			handleSouris = false;
	}
	if(keyboardState[27]){ //ECHAP
		stopSound();
		termineBoucleEvenements();
	}
	if(keyboardState['e']){
		EX.isShown = true;
	}
	if(keyboardState['x']){
		EX.isShown = false;
	}
}
