// DEFINES

#define LF largeurFenetre()
#define HF hauteurFenetre()

#define MOUSE_NOTHING 0
#define MOUSE_LEFT_UP 1
#define MOUSE_LEFT_DOWN 2
#define MOUSE_RIGHT_UP 3
#define MOUSE_RIGHT_DOWN 4
#define MOUSE_SCROLL_UP 5
#define MOUSE_SCROLL_DOWN 6

// TYPEDEFS

typedef struct bouton bouton;

struct bouton{
	int width,height;
	RGB couleur;//zjfuieh
	char *texte;
	int tailleTexte;
	DonneesImageRGB* skin;
	int x,y;
	void (*addEventListener) (bouton *B,char* listener,void (*event) (void));
	void (*event) (void);
	char listener[50];
};

typedef struct textField{
	int width,height;
	RGB couleur;
	char *texte;
	int tailleTexte;
	DonneesImageRGB* skin;
	int x,y;
	void (*event) (void);
	
	bool editing;
	bool showCursor;
	bool isTitle;
} textField;

// FONCTIONS

extern int x1cursor,y1cursor;
extern int x2cursor,y2cursor;
extern int x,y;
extern float xslide,yslide;
extern bool xdraggable,ydraggable;
extern Table T;
extern int tableToLoad ;
extern bool keyboardState[255];
extern bool oneSelected ;
extern int mouseState ;
extern int AS;
extern int OS;
extern bool handleSouris ;
extern textField** TXTF;
extern int nblignetextfield ;
extern int nbcolonnetextfield ;

textField initTextField(int xx,int yy,int width,int height,RGB couleur,char* chaine);
void afficheTextField(textField *B);
void afficheAllTxtFields();
void initTabTextField (Table T);
void textFieldClickListener (textField *B);

void addEventListener (bouton *B,char* listener,void (*event) (void));
bouton initBouton(int width,int height,char* texte,RGB couleur);
bouton initBoutonFromImg(char* chemin,RGB fondVert);
void afficheBouton(int x,int y,bouton *B);
void executeActionBouton (bouton B);

// FONCTIONS USUELLES

void afficheCaractereClavier (void);
void affichePositionSouris (void);
void playSound (char nomFichier[30]);
void stopSound (void);
void playBoucleSound (char nomFichier[30]);
