
typedef struct RGB{
	unsigned char r, g, b;
} RGB;

void lisChaineClavier (char chaine[50],bool *isReadingKey);
//cree par Kyllian MARIE

//ecrisImage mais avec moins de parametres a donner
void ecrisImageShort (DonneesImageRGB *I,int x,int y);
//cree par Kyllian MARIE

//ecrisImageSansFond mais avec moins de parametres a donner
void ecrisImageSFShort (DonneesImageRGB *I,int x,int y,RGB C);
//cree par Kyllian MARIE

// Ecris dans la fenetre une image BVR couvrant largeur*hauteur et demarrant en (x, y) */
void ecrisImageSansFond(int x, int y, int largeur, int hauteur, const unsigned char *donnees, RGB fond);
//cree par Frederique Robert Inacio

void afficheFond(unsigned char R,unsigned char G,unsigned char B);
