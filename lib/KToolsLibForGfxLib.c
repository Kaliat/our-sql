#include <stdlib.h> // Pour pouvoir utiliser exit()
#include <stdio.h> // Pour pouvoir utiliser printf()
#include <math.h> // Pour pouvoir utiliser sin() et cos()
#include "../GfxLib/GfxLib.h" // Seul cet include est necessaire pour faire du graphique
#include "../GfxLib/BmpLib.h" // Cet include permet de manipuler des fichiers BMP
#include "../GfxLib/ESLib.h" // Pour utiliser valeurAleatoire()
#include "stdbool.h"
#include <string.h>
#include "KyllianToolsLib.h"
#include "KToolsLibForGfxLib.h"

//		FONCTIONS GFXLIB		//
// --------------------------------------------------------------------------------------------------------------------------------------- //

//fonction qui lit les characteres tapés au clavier et les ajoute a une chaine
void lisChaineClavier (char chaine[50],bool *isReadingKey){
	if(*isReadingKey == true){
		char a = caractereClavier();
		switch(caractereClavier()){
			case 13:
				*isReadingKey = false;
			break;
			case 8:
				chaine[strlen(chaine)-1] = '\0';
				chaine[strlen(chaine)] = NULL;
			break;
			default:
				if(a >= 40 && a <=126){
					char stra[2];
					stra[0] = a;
					stra[1] = '\0';
					strcat(chaine,stra);
				}
			break;
		}
	}
}
//cree par Kyllian MARIE

//ecrisImage mais avec moins de parametres a donner
void ecrisImageShort (DonneesImageRGB *I,int x,int y){
	ecrisImage(x,y,I->largeurImage,I->hauteurImage,I->donneesRGB);
}
//cree par Kyllian MARIE

//ecrisImageSansFond mais avec moins de parametres a donner
void ecrisImageSFShort (DonneesImageRGB *I,int x,int y,RGB C){
	ecrisImageSansFond(x,y,I->largeurImage,I->hauteurImage,I->donneesRGB,C);
}
//cree par Kyllian MARIE

static int *BVR2ARVB_sansFond(int largeur, int hauteur, const unsigned char *donnees, RGB fond)
{
	const unsigned char *ptrDonnees;
	unsigned char *pixels = (unsigned char*)malloc(largeur*hauteur*sizeof(int));
	unsigned char *ptrPixel;
	int index;
	
	ptrPixel = pixels;
	ptrDonnees = donnees;
	for
		(index = largeur*hauteur; index != 0; --index) /* On parcourt tous les
														pixels de l'image */
	{
		ptrPixel[0] = ptrDonnees[0];
		ptrPixel[1] = ptrDonnees[1];
		ptrPixel[2] = ptrDonnees[2];
		if (ptrDonnees[0] == fond.b && ptrDonnees[1] == fond.g && ptrDonnees[2] == fond.r)
			ptrPixel[3] = 0x000;
		else
			ptrPixel[3] = 0x0FF;
		ptrDonnees += 3; /* On passe a la premiere composante du pixel suivant */
		ptrPixel += 4; /* On passe au pixel suivant */
	}
	return (int*)pixels;
}
//cree par Frederique Robert Inacio

// Ecris dans la fenetre une image BVR couvrant largeur*hauteur et demarrant en (x, y) */
void ecrisImageSansFond(int x, int y, int largeur, int hauteur, const unsigned char *donnees, RGB fond)
{
	int *pixels = BVR2ARVB_sansFond(largeur, hauteur, donnees, fond);
	ecrisImageARVB(x, y, largeur, hauteur, pixels);
	free(pixels);
}
//cree par Frederique Robert Inacio

void afficheFond(unsigned char R,unsigned char G,unsigned char B){
	couleurCourante(R,G,B);
	rectangle(0,0,LF,HF);
}
