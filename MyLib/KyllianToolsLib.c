#include <stdlib.h> // Pour pouvoir utiliser exit()
#include <stdio.h> // Pour pouvoir utiliser printf()
#include <math.h> // Pour pouvoir utiliser sin() et cos()
#include "../../../GfxLib/ESLib.h" // Pour utiliser valeurAleatoire()
#include "stdbool.h"
#include <string.h>
#include "KyllianToolsLib.h"

//fonction pour afficher une chaine en une ligne sans preciser le '\n' utile pour le debugging (fonction inspirée de JAVA)
void println(char* a){
	printf("%s\n",a);
}
//cree par Kyllian MARIE

//retourne le nombre d elements initialisés dans un tableau bidimensionnel d entier
int trueSizeOfIntTab (int **T){
	int a;
	for(a=1;T[a][0] != -1;a++){
	}
	return a;
}
//cree par Kyllian MARIE

//retourne le nombre d elements initialisés dans un tableau d entier
int trueSizeOfInt (int *T){
	int a;
	for(a=0;T[a] != -1;a++){
	}
	return a;
}
//cree par Kyllian MARIE

//retourne true si un fichier donné existe
bool fileExist (char* nomFichier){
	bool a = false;
	FILE* f = fopen(nomFichier,"rt");
	if(f==NULL){a=false;}
	if(f!=NULL){a=true;fclose(f);}
	return(a);
}
//cree par Kyllian MARIE

//fclose seulement si le fichier != NULL
void betterfclose (FILE* f){
	if(f==NULL){}
	if(f!=NULL){fclose(f);}
}
//cree par Kyllian MARIE

//free seulement si le pointeur != NULL
void betterFree (void **ptr){
	if(*ptr!=NULL){free(*ptr);*ptr=NULL;}
	if(*ptr!=NULL){}
}
//cree par Kyllian MARIE

//permet de malloc un tableau a deux dimensions
void** bigMalloc (int size,int DIM1,int DIM2){
	void** ptr = (void**) malloc(sizeof(int*)*DIM1);
	for(int i=0;i<DIM1;i++){
		ptr[i] = (void*) malloc(size*DIM2);
	}
	return ptr;
}
//cree par Kyllian MARIE

//permet de generer une chaine contenant un chemin vers un fichier donné avec un nombre aleatoire
char* genereCheminAleatoire(int nbFichier,char* path,char* extension){
	char* ch;
	char* a;
	float f = 11.;
	int i;
	int nbAleatoire = nbFichier*valeurAleatoire();
	for(i = 0;f>10.;i++){				//i est le nombre de caracteres necessaire pour stocker l'entier sous forme de chaine
		f =(float) nbFichier/(pow(10,i));
	}
	ch = (char*) malloc(sizeof(char)*(1+i) + sizeof(path) + sizeof(extension));
	a = (char*) malloc(sizeof(char)*(1+i));
	strcpy(ch,path);
	sprintf(a,"%d",nbAleatoire);
	strcat(ch,a);
	strcat(ch,extension);
	return ch;
} 
//cree par Kyllian MARIE

void playSoundAleatoire(int nbFichier,char* path,char* extension){
	playSound(genereCheminAleatoire(nbFichier,path,extension));
}
//cree par Kyllian MARIE

void playSound (char nomFichier[30]){
	//POUR UTILISER CETTE FONCTION TAPEZ "sudo apt-get install mpg123" DANS LE TERMINAL
	char command[50];
	strcpy(command,"mpg123 -q ");
	strcat(command,nomFichier);
	strcat(command," &");
	system(command);
}
//cree par Kyllian MARIE

void stopSound (void){
	system("pkill mpg123");
}
//cree par Kyllian MARIE

void playBoucleSound (char nomFichier[30]){
	char command[50];
	strcpy(command,"mpg123 -q --loop -1 ");
	strcat(command,nomFichier);
	strcat(command," &");
	system(command);
}
//cree par Kyllian MARIE

bool not (bool a){
	if(a == true){
		return false;
	}
	else{
		return true;
	}
}
