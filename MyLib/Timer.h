typedef struct timer{
	float t1;
	float t2;
	long t0;
	int tempo;
	bool isStopped;
	bool chrono[100];
	double timeToWait[100];
	bool isWaiting[100];
}timer;

timer resetTimer (void);
void setChrono (timer *T,int port,float timeToWait);
void gereTimer (timer *time);
void stopTimer (timer *time);
void resumeTimer (timer *time);
