#include <stdlib.h> // Pour pouvoir utiliser exit()
#include <stdio.h> // Pour pouvoir utiliser printf()
#include <math.h> // Pour pouvoir utiliser sin() et cos()
#include "GfxLib/GfxLib.h" // Seul cet include est necessaire pour faire du graphique
#include "GfxLib/BmpLib.h" // Cet include permet de manipuler des fichiers BMP
#include "GfxLib/ESLib.h" // Pour utiliser valeurAleatoire()
#include "stdbool.h"
#include <string.h>
#include "KyllianToolsLib.h"

//initialisation d une structure onlinePlayer
onlinePlayer initOP (int id){
	onlinePlayer OP;
	OP.id = id;
	switch(id){
		case 1:
			OP.img = lisBMPRGB("Ressources/Multi/ky.bmp");
			OP.img = resizeDonneesImageRGB(&(OP.img),90);
			sprintf(OP.nom,"-KM");
			printf("%s\n",OP.nom);
		break;
		case 2:
			OP.img = lisBMPRGB("Ressources/Multi/gaian.bmp");
			sprintf(OP.nom,"Xx-DarkGaian-xX");
			OP.img = resizeDonneesImageRGB(&(OP.img),90);
			printf("%s\n",OP.nom);
		break;
		case 3:
			OP.img = lisBMPRGB("Ressources/Multi/tib.bmp");
			sprintf(OP.nom,"TibRib");
			OP.img = resizeDonneesImageRGB(&(OP.img),90);
			printf("%s\n",OP.nom);
		break;
		default:
			println("PREND UN VRAI ID GAIAN");
		break;
	}
	OP.isPlayingWith = false;
	return OP;
}
//cree par Kyllian MARIE

//fonction d affichage des amis 
void afficheOP (onlinePlayer OP1,onlinePlayer OP2,bool show){
	if(show){
		couleurCourante(0,20,40);
		rectangle(LF-400,20,LF,110);
		ecrisImageShort(OP1.img,LF-400,20);
		if(OP1.isConnected){
			couleurCourante(25,255,80);
		}
		else{
			couleurCourante(255,0,0);
		}
		afficheChaine(OP1.nom,36,LF-300,50);
		if(OP1.id == 1){
			afficheChaine("-KM",36,LF-300,50);
		}
		if(OP1.id == 2){
			afficheChaine("Xx-DarkGaian-xX",36,LF-300,50);
		}
		if(OP1.id == 3){
			afficheChaine("TibRib",36,LF-150,50);
		}
		if(OP1.isPlayingWith){
			couleurCourante(0,100,255);
			if(OP1.isMaster == true){
				afficheChaine("Master",18,LF-300,50+36);
			}
			if(OP1.isMaster == false){
				afficheChaine("Suiveur",18,LF-300,50+36);
			}
		}
		
		couleurCourante(0,20,40);
		rectangle(LF-400,140,LF,230);
		ecrisImageShort(OP2.img,LF-400,140);
		if(OP2.isConnected){
			couleurCourante(25,255,80);
		}
		else{
			couleurCourante(255,0,0);
		}
		if(OP2.id == 1){
			afficheChaine("-KM",36,LF-300,160);
		}
		if(OP2.id == 2){
			afficheChaine("Xx-DarkGaian-xX",36,LF-300,160);
		}
		if(OP2.id == 3){
			afficheChaine("TibRib",36,LF-300,160);
		}
	}
}
//cree par Kyllian MARIE

//lis l'entier dans le fichier myID.txt
int lisMyID (void){
	int myID;
	FILE* f = fopen("../../myID.txt","rt");
	if( f== NULL){ return 1;}
	fscanf(f,"%d",&myID);
	betterfclose(f);
	return myID;
}
//cree par Kyllian MARIE

//met a jour le fichier de connexion du dossier partagé multiplayer
void setMyselfOnline (bool a){
	int myID = lisMyID();
	char ch[70];
	sprintf(ch,"/media/sf_partage/partageProjet/Multiplayer/%d.txt",myID);
	FILE* f = fopen(ch,"wt");
	if(f != NULL){
		fprintf(f,"%d",a);
	}
	betterfclose(f);
}
//cree par Kyllian MARIE

//permet de savoir si les amis sont en ligne ou  non
void whoIsOnline(bool *J1,bool *J2){
	FILE* f;
	int a = 1;
	char ch[70];
	for(int i=1;i<4;i++){
		if(i != lisMyID()){
			sprintf(ch,"/media/sf_partage/partageProjet/Multiplayer/%d.txt",i);
			f = fopen(ch,"rt");
			if( f!= NULL){
				if(a==1){
					fscanf(f,"%d",J1); 
				}
				else{
					fscanf(f,"%d",J2); 
				}
				betterfclose(f);
			}
			a++;
			
		}
	}
}
//cree par Kyllian MARIE

void litsenForRequest (onlinePlayer *fromWhom,int* state,int* difficulty,carte C[7][4]){
	char ch[130];
	int a = fromWhom->id*10+lisMyID();
	int b = 0;
	sprintf(ch,"/media/sf_partage/partageProjet/Multiplayer/request2/%d.bin",a);
	FILE* f = fopen(ch,"rb");
	if( f!= NULL){
		fread(&a,sizeof(int),1,f);
		if(a < 10 && a >40){
			//fclose(f);
		}
		if(a >= 10 && a <40){
			println("RRRRRR");
			*state = a*1/10;
			*difficulty = a - *state*10;
			FILE* f2;
			f2 = fopen(ch,"wb");
			fwrite(&b,sizeof(int),1,f2);
			fclose(f2);
		}
		if(a == 40){
			printf("NOT MASTER\n");
			fromWhom->isPlayingWith = true;
			fromWhom->isMaster = true;
			FILE *f3;
			f3 = fopen(ch,"wb");
			fwrite(&b,sizeof(int),1,f3);
			fclose(f3);
		}

		if(a >= 50 && a <60){
			if(a-50 == 2){
				sprintf(fromWhom->nomFichierMemo,"/media/sf_partage/partageProjet/Multiplayer/partieMemory/from%d.bin",fromWhom->id);
				fromWhom->needToLoadMemo = true;
				//enregistrePartie(C,fromWhom->nomFichierMemo);
				FILE *f3;
				f3 = fopen(ch,"wb");
				fwrite(&b,sizeof(int),1,f3);
				fclose(f3);
			}
			if(a-50 == 3){
				sprintf(fromWhom->nomFichierPict,"/media/sf_partage/partageProjet/Multiplayer/dessins/from%d.bin",fromWhom->id);
				fromWhom->needToLoadPict = true;
			}
		}
		fclose(f);
	}
}

void createRequest (int toWhom,int state,int difficulty,carte C[7][4]){
	char ch[130];
	int a = lisMyID()*10+toWhom;
	sprintf(ch,"/media/sf_partage/partageProjet/Multiplayer/request2/%d.bin",a);
	printf("%d,%d,%s\n",lisMyID(),toWhom,ch);
	if(state == 5 && difficulty == 2){
		char ch2[150];
		sprintf(ch2,"/media/sf_partage/partageProjet/Multiplayer/partieMemory/from%d.bin",lisMyID());
		printf("enregistre fichier %s\n",ch2);
		enregistrePartie(C,ch2);
	}
	FILE* f = fopen(ch,"wb");
	if( f!= NULL){
		a = state*10+difficulty;
		printf(">>>>>%d\n",a);
		fwrite(&a,sizeof(int),1,f);
		fclose(f);
	}
}

void loadRequestedFile (onlinePlayer *fromWhom,int currentState,timer* t,carte C[7][4]){
	if(currentState == 2){
		if(fromWhom->needToLoadMemo){
			setChrono(t,84,0.5);
			fromWhom->needToLoadMemo = false;
		}
		if(t->chrono[84]){
			chargePartie(C,fromWhom->nomFichierMemo);
		}
	}
}

