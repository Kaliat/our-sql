#include <stdlib.h> // Pour pouvoir utiliser exit()
#include <stdio.h> // Pour pouvoir utiliser printf()
#include <math.h> // Pour pouvoir utiliser sin() et cos()
#include "GfxLib/GfxLib.h" // Seul cet include est necessaire pour faire du graphique
#include "GfxLib/BmpLib.h" // Cet include permet de manipuler des fichiers BMP
#include "GfxLib/ESLib.h" // Pour utiliser valeurAleatoire()
#include "stdbool.h"
#include <string.h>
#include "KyllianToolsLib.h"

//fonction qui applique resizeDonneesImageRGB a chaque frame d'un gif
void resizeGif(gif* G,int largeur){
	for(int i=0;i<G->numberFrame;i++){
		G->img[i] = resizeDonneesImageRGB(&(G->img[i]),largeur);
	}
}
//cree par Kyllian MARIE

//ALGORITHME DE CROPPING D IMAGE DU "PLUS PROCHE VOISIN"
//cette fonction sert a redimensionner une image en fonction d'une nouvelle largeur (l'image va garder le meme ratio largeur/hauteur)
DonneesImageRGB* resizeDonneesImageRGB (DonneesImageRGB** img,int newLength){	
	DonneesImageRGB* Nimg = (DonneesImageRGB*) malloc(sizeof(DonneesImageRGB));		//nouvelle image
	float coefX;
	coefX = 1./( (*img)->largeurImage*1./newLength );		//coefX represente combien de fois l'image va etre grossie
	int newHeight = (*img)->hauteurImage * coefX;			//newHeight sera la nouvelle hauteur de l'image
	Nimg->hauteurImage = newHeight;							//initialisation manuelle d'une structure DonneesImageRGB
	Nimg->largeurImage = newLength;
	Nimg->donneesRGB = (unsigned char*) malloc(sizeof(unsigned char)*3*newLength*newHeight);
	RGB** T;												//T est un tableau bi-dimensionnel de RGB qui sera utilisé pour stocker l'image initiale(il est plus facile de manipuler un RGB** que Unsigned char*)
	T = (RGB**) malloc(sizeof(RGB*)*(*img)->largeurImage);
	for(int i=0;i<(*img)->largeurImage;i++){
		T[i] = (RGB*) malloc(sizeof(RGB)*(*img)->hauteurImage);
	}
	convertUnsignedCharInRGB(T,(*img)->donneesRGB,(*img)->largeurImage,(*img)->hauteurImage);
	RGB** T2;												//T est un tableau bi-dimensionnel de RGB qui sera utilisé pour stocker l'image convertie
	T2 = (RGB**) malloc(sizeof(RGB*)*newLength);
	for(int i=0;i<newLength;i++){
		T2[i] = (RGB*) malloc(sizeof(RGB)*newHeight);
	}
	int a1;		//abscisse du pixel plus proche voisin
	int a2;		//ordonnee du pixel plus proche voisin
	for(int i=0;i<newLength;i++){
		for(int i2=0;i2<newHeight;i2++){
			a1 = (i*1./coefX);
			a2 = (i2*1./coefX);
			T2[i][i2] = T[a1][a2];
		}
	}
	convertRGBinUnsignedChar(T2,Nimg->donneesRGB,newLength,newHeight);	//reconversion en Unsigned char de la nouvelle image
	//FREE
	for(int i=0;i<(*img)->largeurImage;i++){
		free(T[i]);
		T[i] = NULL;
	}
	free(T);
	T=NULL;
	libereDonneesImageRGB(img);
	*img = NULL;
	//FREE
	return Nimg;
}
//cree par Kyllian MARIE

//fonction de conversion d'une structure RGB** en unsigned char* pour le stockage d'une image
void convertRGBinUnsignedChar (RGB **T,unsigned char* BVR,int largeur,int hauteur){
	int i=0;
	for(int y=0;y<hauteur;y++){
		for(int x=0;x<largeur;x++){
			BVR[i] = T[x][y].b;
			i++;
			BVR[i] = T[x][y].g;
			i++;
			BVR[i] = T[x][y].r;
			i++;
		}
	}
}
//cree par Kyllian MARIE

//fonction de conversion d'une structure unsigned char* en RGB** pour le stockage d'une image
void convertUnsignedCharInRGB (RGB **T,unsigned char* BVR,int largeur,int hauteur){
	int i=0;
	for(int y=0;y<hauteur;y++){
		for(int x=0;x<largeur;x++){
			T[x][y].b = BVR[i];
			i++;
			T[x][y].g = BVR[i];
			i++;
			T[x][y].r = BVR[i];
			i++;
		}
	}
}
//cree par Kyllian MARIE

gif initGif (char folderName[20],char commonName[20],int numberFrame,int fps,bool backExist,couleur color){
	//CETTE FONCTION SERT A DEFINIR UNE SEQUENCE D IMAGE POUR POUVOIR L UTILISER PLUS TARD
	//CES SEQUENCES D IMAGES NE SONT PAS DES .GIF MAIS ONT LES MEMES PROPRIETES ET BUT QUE LES .GIFS
	gif G;
	char base[40];		//BASE DU CHEMIN D ACCES AUX FICHIERS IMAGE
	char a[4];			//CHAINE OU EST STOCKé LE NUMERO DE L IMAGE A CHARGER
	char chemin[50];	//CHAINE CONTENANT LE CHEMIN D ACCES COMPLET A UN FICHIER
	// CREATION DE LA BASE DE LA CHAINE
	strcpy(base,"Ressources/");
	strcat(base,folderName);
	strcat(base,"/");
	strcat(base,commonName);
	//ALLOCATION DE LA MEMOIRE NECESSAIRE A UN TABLEAU CONTENANT LE NOMBRE D IMAGE REQUIS POUR L ANIMATION
	G.img = (DonneesImageRGB**) malloc(sizeof(DonneesImageRGB)*numberFrame);
	for(int i=0;i<numberFrame;i++){
		//CREATION DE LA CHAINE CHEMIN D ACCES COMPLET ET LECTURE DE L IMAGE ASSOCIEE A CHAQUE FRAME DU "GIF"
		sprintf(a,"%d",i);
		strcpy(chemin,base);
		strcat(chemin,a);
		strcat(chemin,".bmp");
		G.img[i] = lisBMPRGB(chemin);
	}
	//INITIALISATION BASIQUE DE LA STRUCTURE GIF
	G.numberFrame = numberFrame;
	G.backgroundExist = backExist;
	G.background = color;
	G.currentFrame = 0;
	G.fps = fps;
	G.pas = ((20*0.001))*fps;
	G.lastTime = 0;
	G.isFrozen = false;
	G.boucle = 0;
	return G;
}
//cree par Kyllian MARIE

void afficheGif(gif *G,int x,int y,bool sens,timer T){
	//CETTE FONCTION SERT A AFFICHER UN GIF IMAGE PAR IMAGE 
	if(G->currentFrame >= G->numberFrame){G->currentFrame=0; (G->boucle)++;	} 	//SI ON A ATTEIND LA DERNIERE FRAME ON REBOUCLE A LA PREMIERE
	if(G->currentFrame < 0){G->currentFrame = G->numberFrame-1; (G->boucle)++; }
	int currentFrame = G->currentFrame;
	ecrisImageSansFond(		//AFFICHAGE DE L IMAGE
		x,
		y,
		(G->img[currentFrame])->largeurImage,
		(G->img[currentFrame])->hauteurImage,
		(G->img[currentFrame])->donneesRGB,
		G->background);
	if(G->lastTime != 0 && G->isFrozen == false){
		if(sens == true){
			G->currentFrame = G->currentFrame + G->pas*(((T.J1+T.J2+T.J3+T.menu) - G->lastTime )*1./(20*0.001));
								//PASSAGE A LA FRAME SUIVANTE 
		}
		if(sens == false){
			G->currentFrame = G->currentFrame - G->pas*(((T.J1+T.J2+T.J3+T.menu) - G->lastTime )*1./(20*0.001));
		}
	}
	G->lastTime = T.J1+T.J2+T.J3+T.menu;
}
//cree par Kyllian MARIE
