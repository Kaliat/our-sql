#define LF largeurFenetre()
#define HF hauteurFenetre()
#define NOIR 0,0,0
#define BLANC 255,255,255
//#define AS abscisseSouris()
//#define OS ordonneeSouris()

//fonction pour afficher une chaine en une ligne sans preciser le '\n' utile pour le debugging (fonction inspirée de JAVA)
void println(char* a);
//cree par Kyllian MARIE

//retourne le nombre d elements initialisés dans un tableau bidimensionnel d entier
int trueSizeOfIntTab (int **T);
//cree par Kyllian MARIE

//retourne le nombre d elements initialisés dans un tableau d entier
int trueSizeOfInt (int *T);
//cree par Kyllian MARIE

//retourne true si un fichier donné existe
bool fileExist (char* nomFichier);
//cree par Kyllian MARIE

//fclose seulement si le fichier != NULL
void betterfclose (FILE* f);
//cree par Kyllian MARIE

//free seulement si le pointeur != NULL
void betterFree (void **ptr);
//cree par Kyllian MARIE

//permet de malloc un tableau a deux dimensions
void** bigMalloc (int size,int DIM1,int DIM2);
//cree par Kyllian MARIE

//permet de generer une chaine contenant un chemin vers un fichier donné avec un nombre aleatoire
char* genereCheminAleatoire(int nbFichier,char* path,char* extension);
//cree par Kyllian MARIE

void playSoundAleatoire(int nbFichier,char* path,char* extension);
//cree par Kyllian MARIE

void playSound (char nomFichier[30]);
//cree par Kyllian MARIE

void stopSound (void);
//cree par Kyllian MARIE

void playBoucleSound (char nomFichier[30]);
//cree par Kyllian MARIE

bool not (bool a);
