#include <stdlib.h> // Pour pouvoir utiliser exit()
#include <stdio.h> // Pour pouvoir utiliser printf()
#include <math.h> // Pour pouvoir utiliser sin() et cos()
#include "../bddGraph/GfxLib/GfxLib.h" // Seul cet include est necessaire pour faire du graphique
#include "../bddGraph/GfxLib/BmpLib.h" // Cet include permet de manipuler des fichiers BMP
#include "../bddGraph/GfxLib/ESLib.h" // Pour utiliser valeurAleatoire()
#include "stdbool.h"
#include <string.h>
#include "KyllianToolsLib.h"
#include "KToolsLibForGfxLib.h"

//		FONCTIONS GFXLIB		//
// --------------------------------------------------------------------------------------------------------------------------------------- //

//fonction de conversion d'une structure RGB** en unsigned char* pour le stockage d'une image
void convertRGBinUnsignedChar (RGB **T,unsigned char* BVR,int largeur,int hauteur){
	int i=0;
	for(int y=0;y<hauteur;y++){
		for(int x=0;x<largeur;x++){
			BVR[i] = T[x][y].b;
			i++;
			BVR[i] = T[x][y].g;
			i++;
			BVR[i] = T[x][y].r;
			i++;
		}
	}
}
//cree par Kyllian MARIE

//fonction de conversion d'une structure unsigned char* en RGB** pour le stockage d'une image
void convertUnsignedCharInRGB (RGB **T,unsigned char* BVR,int largeur,int hauteur){
	int i=0;
	for(int y=0;y<hauteur;y++){
		for(int x=0;x<largeur;x++){
			T[x][y].b = BVR[i];
			i++;
			T[x][y].g = BVR[i];
			i++;
			T[x][y].r = BVR[i];
			i++;
		}
	}
}
//cree par Kyllian MARIE

DonneesImageRGB* resizeDonneesImageRGB (DonneesImageRGB** img,int newLength){	
	DonneesImageRGB* Nimg = (DonneesImageRGB*) malloc(sizeof(DonneesImageRGB));		//nouvelle image
	float coefX;
	coefX = 1./( (*img)->largeurImage*1./newLength );		//coefX represente combien de fois l'image va etre grossie
	int newHeight = (*img)->hauteurImage * coefX;			//newHeight sera la nouvelle hauteur de l'image
	Nimg->hauteurImage = newHeight;							//initialisation manuelle d'une structure DonneesImageRGB
	Nimg->largeurImage = newLength;
	Nimg->donneesRGB = (unsigned char*) malloc(sizeof(unsigned char)*3*newLength*newHeight);
	RGB** T;												//T est un tableau bi-dimensionnel de RGB qui sera utilisé pour stocker l'image initiale(il est plus facile de manipuler un RGB** que Unsigned char*)
	T = (RGB**) malloc(sizeof(RGB*)*(*img)->largeurImage);
	for(int i=0;i<(*img)->largeurImage;i++){
		T[i] = (RGB*) malloc(sizeof(RGB)*(*img)->hauteurImage);
	}
	convertUnsignedCharInRGB(T,(*img)->donneesRGB,(*img)->largeurImage,(*img)->hauteurImage);
	RGB** T2;												//T est un tableau bi-dimensionnel de RGB qui sera utilisé pour stocker l'image convertie
	T2 = (RGB**) malloc(sizeof(RGB*)*newLength);
	for(int i=0;i<newLength;i++){
		T2[i] = (RGB*) malloc(sizeof(RGB)*newHeight);
	}
	int a1;		//abscisse du pixel plus proche voisin
	int a2;		//ordonnee du pixel plus proche voisin
	for(int i=0;i<newLength;i++){
		for(int i2=0;i2<newHeight;i2++){
			a1 = (i*1./coefX);
			a2 = (i2*1./coefX);
			T2[i][i2] = T[a1][a2];
		}
	}
	convertRGBinUnsignedChar(T2,Nimg->donneesRGB,newLength,newHeight);	//reconversion en Unsigned char de la nouvelle image
	//FREE
	for(int i=0;i<(*img)->largeurImage;i++){
		free(T[i]);
		T[i] = NULL;
	}
	free(T);
	T=NULL;
	libereDonneesImageRGB(img);
	*img = NULL;
	//FREE
	return Nimg;
}

//fonction qui lit les characteres tapés au clavier et les ajoute a une chaine
void lisChaineClavier (char chaine[50],bool *isReadingKey){
	if(*isReadingKey == true){
		char a = caractereClavier();
		switch(caractereClavier()){
			case 13:
				*isReadingKey = false;
			break;
			case 8:
				chaine[strlen(chaine)-1] = '\0';
				chaine[strlen(chaine)] = NULL;
			break;
			default:
				if(a >= 40 && a <=126){
					char stra[2];
					stra[0] = a;
					stra[1] = '\0';
					strcat(chaine,stra);
				}
			break;
		}
	}
}
//cree par Kyllian MARIE

//ecrisImage mais avec moins de parametres a donner
void ecrisImageShort (DonneesImageRGB *I,int x,int y){
	ecrisImage(x,y,I->largeurImage,I->hauteurImage,I->donneesRGB);
}
//cree par Kyllian MARIE

//ecrisImageSansFond mais avec moins de parametres a donner
void ecrisImageSFShort (DonneesImageRGB *I,int x,int y,RGB C){
	ecrisImageSansFond(x,y,I->largeurImage,I->hauteurImage,I->donneesRGB,C);
}
//cree par Kyllian MARIE

static int *BVR2ARVB_sansFond(int largeur, int hauteur, const unsigned char *donnees, RGB fond)
{
	const unsigned char *ptrDonnees;
	unsigned char *pixels = (unsigned char*)malloc(largeur*hauteur*sizeof(int));
	unsigned char *ptrPixel;
	int index;
	
	ptrPixel = pixels;
	ptrDonnees = donnees;
	for
		(index = largeur*hauteur; index != 0; --index) /* On parcourt tous les
														pixels de l'image */
	{
		ptrPixel[0] = ptrDonnees[0];
		ptrPixel[1] = ptrDonnees[1];
		ptrPixel[2] = ptrDonnees[2];
		if (ptrDonnees[0] == fond.b && ptrDonnees[1] == fond.g && ptrDonnees[2] == fond.r)
			ptrPixel[3] = 0x000;
		else
			ptrPixel[3] = 0x0FF;
		ptrDonnees += 3; /* On passe a la premiere composante du pixel suivant */
		ptrPixel += 4; /* On passe au pixel suivant */
	}
	return (int*)pixels;
}
//cree par Frederique Robert Inacio

// Ecris dans la fenetre une image BVR couvrant largeur*hauteur et demarrant en (x, y) */
void ecrisImageSansFond(int x, int y, int largeur, int hauteur, const unsigned char *donnees, RGB fond)
{
	int *pixels = BVR2ARVB_sansFond(largeur, hauteur, donnees, fond);
	ecrisImageARVB(x, y, largeur, hauteur, pixels);
	free(pixels);
}
//cree par Frederique Robert Inacio

void afficheFond(unsigned char R,unsigned char G,unsigned char B){
	couleurCourante(R,G,B);
	rectangle(0,0,LF,HF);
}
