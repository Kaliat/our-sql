#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "../GfxLib/ESLib.h"
#include "stdbool.h"
#include "Timer.h"

//fonction d initialisation de la structure timer
timer resetTimer (void){
	timer time;
	time.t0 = tempsReel();
	time.t1 = 0;
	time.t2 = 0;
	time.isStopped = false;
	time.tempo = 20;
	for(int i=0;i<100;i++){
		time.isWaiting[i] = false;
		time.timeToWait[i] = 0;
		time.chrono[i] = false;
	}
	return time;
}
//cree par Kyllian MARIE

//fonction qui permet de lancer un chronometre pour executer une action dans le temps specifié
void setChrono (timer *T,int port,float timeToWait){
	T->isWaiting[port] = true;
	T->timeToWait[port] = tempsReel()+timeToWait;
}
//cree par Kyllian MARIE

//fonction de gestion d un timer
void gereTimer (timer *time){
	if(time->isStopped){
		time->t2 = tempsReel() - time->t0 - time->t1;
	}
	else{
		//on incremente le temps passé dans un jeu si e joueur joue a ce jeu
		time->t1 = tempsReel() - time->t0 - time->t2;
	}
	for(int i=0;i<100;i++){
		time->chrono[i] = false;
		if(time->isWaiting[i]){
			//si on a atteint le temps a attendre du chrono on passe la valeur du booleen associé au chrono a true
			if(time->timeToWait[i] <= tempsReel()){
				time->isWaiting[i] = false;
				time->timeToWait[i] = 0;
				time->chrono[i] = true;
			}
		}
	}
}
//cree par Kyllian MARIE

//fonction d arret du timer
void stopTimer (timer *time){
	time->isStopped = true;
}
//cree par Kyllian MARIE

//fonction de continuation du timer
void resumeTimer (timer *time){
	time->isStopped = false;
}
//cree par Kyllian MARIE
