#include <stdlib.h> // Pour pouvoir utiliser exit()
#include <stdio.h> // Pour pouvoir utiliser printf()
#include <math.h> // Pour pouvoir utiliser sin() et cos()
#include "../bddGraph/GfxLib/GfxLib.h" // Seul cet include est necessaire pour faire du graphique
#include "../bddGraph/GfxLib/BmpLib.h" // Cet include permet de manipuler des fichiers BMP
#include "../bddGraph/GfxLib/ESLib.h" // Pour utiliser valeurAleatoire()
#include "stdbool.h"
#include <string.h>
#include "KyllianToolsLib.h"
#include "KToolsLibForGfxLib.h"
#include "exploFichier.h"

#define AS abscisseSouris()
#define OS ordonneeSouris()

void afficheExploFichier (exploFichier EX){
	if(EX.isShown == true){
		ecrisImageShort(EX.img,LF/2-((EX.img)->largeurImage)/2,HF/2-((EX.img)->hauteurImage)/2);
		couleurCourante(0,0,0);
		afficheChaine(EX.currentDir,38,LF/2-((EX.img)->largeurImage)/2 + 139,HF/2-((EX.img)->hauteurImage)/2 + 711);
		int a;
		if((EX.page+1)*16 < EX.nbChaine){
			a = (EX.page+1)*16;
		}
		else{
			a = EX.nbChaine;
		}
		for(int i=EX.page*16;i<a;i++){
			if(EX.F[i].typeFichier != TYPE_INCONNU){
				ecrisImageSFShort(EX.imgTypeFichier[EX.F[i].typeFichier],EX.F[i].box[0].x - 70,EX.F[i].box[0].y,(RGB){255,0,0});
				couleurCourante(0,0,0);
				afficheChaine(EX.F[i].nom,30,EX.F[i].box[0].x,EX.F[i].box[0].y);
				afficheChaine(EX.F[i].taille,30,EX.F[i].box[0].x+684,EX.F[i].box[0].y);
				afficheChaine(EX.F[i].time,30,EX.F[i].box[0].x+950,EX.F[i].box[0].y);
			}
		}
	}
	else{
		if(!(EX.openedImage == NULL)){
			println("BUB");
			printf("%d\n",EX.openedImage->largeurImage);
			//ecrisImageShort(EX.openedImage,0,0);
			println("BAB");
		}
	}
}


//fonction qui permet d'identifier le type d'un fichier a partir de son nom
void comprendTypeFichier (fichier *F){
	F->typeFichier =  TYPE_INCONNU;
	if(F->nom[strlen(F->nom)-4] == '.' && F->nom[strlen(F->nom)-3] == 't' && F->nom[strlen(F->nom)-2] == 'x' && F->nom[strlen(F->nom)-1] == 't'){
		//si le nom se termine par ".bmp"
		F->typeFichier = TYPE_TXT;
	}
	if(F->nom[strlen(F->nom)-5] == '.' && F->nom[strlen(F->nom)-4] == 'o' && F->nom[strlen(F->nom)-3] == 's' && F->nom[strlen(F->nom)-2] == 'q' && F->nom[strlen(F->nom)-1] == 'l'){
		//si le nom se termine par ".bmpt"
		F->typeFichier =  TYPE_OSQL;
	}
	if(F->nom[strlen(F->nom)-5] == '.' && F->nom[strlen(F->nom)-4] == 'd' && F->nom[strlen(F->nom)-3] == 'a' && F->nom[strlen(F->nom)-2] == 't' && F->nom[strlen(F->nom)-1] == 'a'){
		//si le nom se termine par ".data"
		F->typeFichier =  TYPE_DATA;
	}
	if(contientUnPoint(F->nom) == false && F->taille[0] == '4' && F->taille[1] == 'K'){
		//si le nom ne contient pas de point et que la taille du fichier est de 4Ko alors on en deduit que ce fichier est un dossier
		F->typeFichier =  TYPE_FOLDER;
	}
}
//cree par Kyllian MARIE

//fonction qui return true qi la chaine donnée contient un '.' false sinon
bool contientUnPoint (char* ch){
	bool a = false;
	for(int i=0;i<strlen(ch);i++){
		if(ch[i] == '.'){
			a = true;
		}
	}
	return a;
}
//cree par Kyllian MARIE

//fonction qui si on lui donne un chemin type "/home/yncrea/monDossier" va retourner la meme chaine en remontant d'un cran soit "/home/yncrea"
char* removeLastFolder (char* ch){
	char* C2;
	int i;
	C2 = (char*) malloc(260*sizeof(char));
	for(i = strlen(ch);ch[i] != '/';i--){}	//i est un compteur du nombre de characteres de la chaine jusqu'au dernier '/' qu'elle contient
	for(int i2=0;i2<i;i2++){
		//on copie la chaine jusqu'au dernier '/'
		C2[i2] = ch[i2];
	}
	C2[i] = '\0';		//on n'oublie pas le '\0' pour "fermer" la chaine
	betterFree((void**)&ch);
	return C2;
}
//cree par Kyllian MARIE

//fonction permettant d'interpreter la chaine retournée par la commande "ls -i [emplacement]" et d'en extraire les informations
void InterpreteLSargumentL (char* C,char* nom,char* taille,char* timeFull){
	char* C2;
	C2 = (char*) malloc(260*sizeof(char));
	int i2=0;
	//on enleve le charactere 32 soit [ESPACE] si il y a un autre [ESPACE] avant lui (ls -i retourne une mise en forme telle que le nombre d espace entre chaque champs n'est pas constant)
	for(int i=0;i<strlen(C);i++){
		if(C[i] != 32){
			C2[i2] = C[i];
			i2++;
		}
		if(C[i] == 32 && C[i-1] != 32){
			C2[i2] = 32;
			i2++;
		}
	}
	C2[i2] = '\0';
	i2=0;
	char* chINUTILE1 = (char*) malloc(sizeof(char)*260);
	char* chINUTILE2 = (char*) malloc(sizeof(char)*260);
	char* chINUTILE3 = (char*) malloc(sizeof(char)*260);
	char* chINUTILE4 = (char*) malloc(sizeof(char)*260);
	char* mois = (char*) malloc(sizeof(char)*260);
	char* jour = (char*) malloc(sizeof(char)*260);
	char* heure = (char*) malloc(sizeof(char)*260);
	
	sscanf(C2,"%s %s %s %s %s %s %s %s %s",chINUTILE1,chINUTILE2,chINUTILE3,chINUTILE4,taille,mois,jour,heure,nom);
	for(int i=strlen(chINUTILE1)+strlen(chINUTILE2)+strlen(chINUTILE3)+strlen(chINUTILE4)+strlen(mois)+strlen(jour)+strlen(heure)+strlen(taille)+8;i<strlen(C2);i++){
		nom[i2] = C2[i];
		i2++;
	}
	nom[i2] = '\0';
	//timeFULL doit contenir le jour le mois et l annee dans une seule chaine
	strcpy(timeFull,jour);
	strcat(timeFull," ");
	strcat(timeFull,mois);
	strcat(timeFull," a ");
	strcat(timeFull,heure);
	
	//la taille retournée par ls -i est une taille en octet on en deduit donc l'unité qui sera affichée (Byte,KiloByte, ... ,GigaByte)
	if(strlen(taille) >= 10){		
		taille[strlen(taille)-10+1] = 'G';
		taille[strlen(taille)-10+2] = 'o';
		taille[strlen(taille)-10+3] = '\0';
	}
	else{ if(strlen(taille) >= 7){
			taille[strlen(taille)-7+1] = 'M';
			taille[strlen(taille)-7+2] = 'o';
			taille[strlen(taille)-7+3] = '\0';
		}
		else{ if(strlen(taille) >= 4){
				taille[strlen(taille)-4+1] = 'K';
				taille[strlen(taille)-4+2] = 'o';
				taille[strlen(taille)-4+3] = '\0';
			}
			else{
				taille[strlen(taille)] = 'B';
				taille[strlen(taille)+1] = '\0';
			}
		}
	}
	//on libere les chaines
	free(chINUTILE1);
	chINUTILE1 = NULL;
	free(chINUTILE2);
	chINUTILE2 = NULL;
	free(chINUTILE3);
	chINUTILE3 = NULL;
	free(chINUTILE4);
	chINUTILE4 = NULL;
	free(mois);
	mois = NULL;
	free(jour);
	jour = NULL;
	free(heure);
	heure = NULL;
	free(C2);
	C2 = NULL;
}
//cree par Kyllian MARIE

//fonction permettant d'initialiser la structure ExploFichier a partir d'un chemin type /home/
exploFichier initExploFichier (char* chemin){
	exploFichier EX;
	int x = 126;		//position du nom d'un fichier a afficher relatif a la fenetre de l explorateur de fichier
	int y = 126;
	EX.isRefreshed = false;
	EX.isShown = false;
	strcpy(EX.currentDir,chemin);
	EX.F = (fichier*) malloc(sizeof(fichier)*10);
	EX.nbChMax = 10;	//nombre maximal de fichier pouvant etre affiché (pourra etre modifié plus tard en reallouant de la memoire a EX.F)
	for(int i=0;i<10;i++){	//initialisation de la structure fichier
		(EX.F[i]).nom = (char*) malloc(sizeof(char)*260);
		(EX.F[i]).nom[0] = '?';
		(EX.F[i]).fullInfo = (char*) malloc(sizeof(char)*260);
		(EX.F[i]).taille = (char*) malloc(sizeof(char)*260);
		(EX.F[i]).time = (char*) malloc(sizeof(char)*260);
	}
	lisListeFichier(&(EX.F),EX.currentDir,&(EX.nbChaine),&(EX.nbChMax));
	EX.img = lisBMPRGB("Ressources/explofichier/explofichier.bmp");
	EX.imgTypeFichier[TYPE_INCONNU] = lisBMPRGB("Ressources/explofichier/x.bmp");
	EX.imgTypeFichier[TYPE_DATA] = lisBMPRGB("Ressources/explofichier/data.bmp");
	EX.imgTypeFichier[TYPE_TXT] = lisBMPRGB("Ressources/explofichier/img.bmp");
	EX.imgTypeFichier[TYPE_FOLDER] = lisBMPRGB("Ressources/explofichier/folder.bmp");
	EX.imgTypeFichier[TYPE_OSQL] = lisBMPRGB("Ressources/explofichier/bmpt.bmp");
	for(int i=0;i<EX.nbChaine;i++){
		comprendTypeFichier(&(EX.F[i]));
		if((EX.F[i]).typeFichier != TYPE_INCONNU){
			if(i%16 == 0){
				y = 126;
			}
			y += (30+5);
			(EX.F[i]).box[0].x = LF/2 - ((EX.img)->largeurImage)/2 + x;
			(EX.F[i]).box[0].y = HF/2 + ((EX.img)->hauteurImage)/2 - y;
			(EX.F[i]).box[1].x = EX.F[i].box[0].x + tailleChaine(EX.F[i].nom,30);
			(EX.F[i]).box[1].y = EX.F[i].box[0].y + 30;
		}
	}
	EX.nbPage = EX.nbChaine*1./16;
	if(EX.nbChaine%16 != 0){
		(EX.nbPage)++;
	}
	EX.page = 0;
	EX.openedImage = NULL;
	return EX;
}
//cree par Kyllian MARIE

//fonction permettant de gerer un clic sur l explorateur de fichier
void gereClicExploFichier (exploFichier* EX,char** fileToLoad,int ASx,int OSx,bool clickState){
	if(EX->isShown && clickState){
		int a;
		if((EX->page+1)*16 < EX->nbChaine){
			a = (EX->page+1)*16;
		}
		else{
			a = EX->nbChaine;
		}
		for(int i=EX->page*16;i<a;i++){
			if(ASx>=EX->F[i].box[0].x && OSx>=EX->F[i].box[0].y && ASx<=EX->F[i].box[1].x && OSx<=EX->F[i].box[1].y){
				if(EX->F[i].typeFichier == TYPE_FOLDER){
					//si un dossier est selectionné on se place dans ce dossier
					EX->isRefreshed = true;
					EX->selection = i;
				}
				if(EX->F[i].typeFichier == TYPE_OSQL){
					char* pathFichier = (char*) malloc(sizeof(char)*300);
					strcpy(pathFichier,EX->currentDir);
					strcat(pathFichier,"/");
					strcat(pathFichier,EX->F[i].nom);
					*fileToLoad = pathFichier;
				}
				if(EX->F[i].typeFichier == TYPE_TXT){
					char* pathFichier = (char*) malloc(sizeof(char)*300);
					strcpy(pathFichier,EX->currentDir);
					strcat(pathFichier,"/");
					strcat(pathFichier,EX->F[i].nom);
					*fileToLoad = pathFichier;
				}
			}
		}
		if(ASx >= LF/2 - ((EX->img)->largeurImage)/2 + 18 && ASx <= LF/2 - ((EX->img)->largeurImage)/2 + 81 && OSx >= HF/2 - ((EX->img)->hauteurImage)/2 + 703 && OSx <= HF/2 - ((EX->img)->hauteurImage)/2 + 760){
			//si on a clické sur le bouton "../" on remonte d un cran dans l arborescence des fichiers
			EX->isRefreshed = true;
			EX->selection = -1;
		}
		if(ASx >= LF/2 - ((EX->img)->largeurImage)/2 + 116 && ASx <= LF/2 - ((EX->img)->largeurImage)/2 + 403 && OSx >= HF/2 - ((EX->img)->hauteurImage)/2 + 24 && OSx <= HF/2 - ((EX->img)->hauteurImage)/2 + 54){
			//si on a appuyé sur le bouton "bas" on se prepare a afficher les fichiers de la page suivante
			if(0 <= EX->page-1){
				(EX->page)--;
			}
		}
		if(ASx >= LF/2 - ((EX->img)->largeurImage)/2 + 437 && ASx <= LF/2 - ((EX->img)->largeurImage)/2 + 772 && OSx >= HF/2 - ((EX->img)->hauteurImage)/2 + 24 && OSx <= HF/2 - ((EX->img)->hauteurImage)/2 + 54){
			//si on a appuyé sur le bouton "haut" on se prepare a afficher les fichiers de la page precedente
			if(EX->nbPage >= EX->page+2){
				(EX->page)++;
			}
		}
	}
}
//cree par Kyllian MARIE

//fonction de gestion de l'explorateur de fichiers
void gereExploFichier (exploFichier* EX){
	if(EX->isRefreshed){	//on rafraichis l'exlorateur de fichiers si besoin
		EX->isRefreshed = false;
		char* dir = malloc(sizeof(char)*250);
		if(EX->selection != -1){
			//si on a selectionné "../" on reinitialise l explorateur de fichier dans le chemin superieur
			strcpy(dir,EX->currentDir);
			strcat(dir,"/");
			strcat(dir,(EX->F[EX->selection]).nom);
		}
		if(EX->selection == -1){
			//si on a selectionné un dossier on se place dans ce dossier
			strcpy(dir,EX->currentDir);
			dir = removeLastFolder(dir);
		}
		freeExploFichier(EX);
		*EX = initExploFichier(dir);
		EX->isShown = true;
	}
}
//cree par Kyllian MARIE

//fonction de liberation de memoire de l explorateur de fichier
void freeExploFichier (exploFichier* EX){
	for(int i=0;i<EX->nbChaine;i++){
		betterFree((void**)&(EX->F[i].nom));
		betterFree((void**)&(EX->F[i].fullInfo));
		betterFree((void**)&(EX->F[i].taille));
		betterFree((void**)&(EX->F[i].time));
	}
	betterFree((void**)&(EX->F));
	for(int i=0;i<5;i++){
		betterFree((void**)&(EX->imgTypeFichier[i]));
	}
	betterFree((void**)&(EX->img));
}
//cree par Kyllian MARIE

//REMPLIS UN TABLEAU DE CHAINES DE CHARACTERES AVEC LES NOMS DES FICHIERS PROVENANT DU CHEMIN DONNE
void lisListeFichier(fichier** lsFile,char* chemin,int* nbChaine,int* nbChMax){		
	*nbChaine = 0;
	FILE *file;
	char elements[800];
	char *chFull;
	
	fichier fichierTest;
	fichierTest.nom = (char*) malloc(sizeof(char)*260);
	fichierTest.taille = (char*) malloc(sizeof(char)*260);
	fichierTest.time = (char*) malloc(sizeof(char)*260);
	
	chFull = (char*) malloc(sizeof(char)*100 + sizeof(chemin));
	sprintf(chFull,"/bin/ls -l [%s[",chemin);	//on met des '[' pour qu'ils soient remplacés par des guillemets afin que le chemin donné a ls soit entre guillemets pour eviter tout probleme avec les espaces
	for(int i=0;i<strlen(chFull);i++){
		if(chFull[i] == '['){
			chFull[i] = 34;
		}
	}
	file = popen(chFull,"r");						
	if (file == NULL) {
		printf("Commande pas executée\n" );
	}
	else{
		int i = 0;
		for(int i3=0;fgets(elements,sizeof(elements)-2,file) != NULL;i3++) {
			if(i3>=1){
				InterpreteLSargumentL(elements,fichierTest.nom,fichierTest.taille,fichierTest.time);
				fichierTest.nom = enleveBSn(fichierTest.nom);
				comprendTypeFichier(&fichierTest);
				if(fichierTest.typeFichier != TYPE_INCONNU){
					*nbChaine = *nbChaine+1;
					if(i >= *nbChMax){	//si on depasse le nombre maximal de fichiers stockable dans explofichier on rajoute 10 emplacements dans le tableau de fichiers
						*lsFile = (fichier*) realloc(*lsFile,*nbChMax*sizeof(fichier)+10*sizeof(fichier));
						for(int i2=0;i2<10;i2++){
							((*lsFile)[i+i2]).nom = (char*) malloc(sizeof(char)*260);
							((*lsFile)[i+i2]).fullInfo = (char*) malloc(sizeof(char)*260);
							((*lsFile)[i+i2]).taille = (char*) malloc(sizeof(char)*260);
							((*lsFile)[i+i2]).time = (char*) malloc(sizeof(char)*260);
						}
						(*nbChMax)+=10;
					}
					strcpy((*lsFile)[i].fullInfo,"");
					strcat((*lsFile)[i].fullInfo,elements);
					(*lsFile)[i].fullInfo = enleveBSn((*lsFile)[i].fullInfo);
					InterpreteLSargumentL((*lsFile)[i].fullInfo,(*lsFile)[i].nom,(*lsFile)[i].taille,(*lsFile)[i].time);
					i++;
				}
			}
		}
	  pclose(file);	
	}
}
//cree par Kyllian MARIE

//fonction qui enleve le dernier charactere d une chaine
char* enleveBSn (char* C){
	char* C2;
	int i;
	C2 = (char*) malloc(260*sizeof(char));
	for(i=0;i<strlen(C)-1;i++){
		C2[i] = C[i];
	}
	C2[i] = '\0';
	return C2;
}
//cree par Kyllian MARIE

//fonction qui remplace les espaces d une chaine par \[ESPACE]
char* corrigeChaineEspace (char* C){
	char* C2;
	int i2=0;
	C2 = (char*) malloc(sizeof(C)+12*sizeof(char));
	for(int i=0;i<strlen(C);i++){
		if(C[i] != ' '){
			C2[i2] = C[i];
		}
		if(C[i] == ' '){
			C2[i2] = 92;	//Valeur ASCII du '\'
			i2++;
			C2[i2] = ' ';
		}
		i2++;
	}
	C2[i2] = '\0';
	free(C);
	C=NULL;
	return C2;
}
//cree par Kyllian MARIE

//fonction qui rajoute des guillemets au debut et a la fin d une chaine
char* putInQuotes (char* C){
	char* C2;
	int i2=0;
	C2 = (char*) malloc(sizeof(C)+12*sizeof(char));
	C2[0] = 34;				// ASCII DU ' " '
	i2++;
	for(int i=0;i<strlen(C);i++){
		C2[i2] = C[i];
		i2++;
	}
	C2[i2] = 34;
	C2[i2+1] = '\0';
	free(C);
	C=NULL;
	return C2;
}
//cree par Kyllian MARIE
